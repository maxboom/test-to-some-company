<?php
declare(strict_types=1);

namespace App\Entity;

/**
 * Class Request
 *
 * @package App\Entity
 */
class Request
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var null|string
     */
    private $body;

    /**
     * @var null|string
     */
    private $route;

    /**
     * @var null|int
     */
    private $method;

    /**
     * @var null|string
     */
    private $ip;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set headers.
     *
     * @param array $headers
     *
     * @return Request
     */
    public function setHeaders(array $headers): Request
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get headers.
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Set body.
     *
     * @param array $body
     *
     * @return Request
     */
    public function setBody(string $body): Request
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * Set route.
     *
     * @param string $route
     *
     * @return Request
     */
    public function setRoute(string $route): Request
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route.
     *
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * Set method.
     *
     * @param int $method
     *
     * @return Request
     */
    public function setMethod(int $method): Request
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method.
     *
     * @return int
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Set ip.
     *
     * @param string $ip
     *
     * @return Request
     */
    public function setIp(string $ip): Request
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }
}
